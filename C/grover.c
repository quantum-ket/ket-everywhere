#include <kbw.h>
#include <libket.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

void check(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    const char* msg = ket_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

void check_kbw(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    const char* msg = kbw_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

void grover(int size) {
  struct timeval begin, end;
  gettimeofday(&begin, NULL);

  ket_process_t p;
  check(ket_process_new(size, &p));

  ket_qubit_t* qubits = malloc(sizeof(ket_qubit_t) * size);

  for (int i = 0; i < size; i++) {
    check(ket_process_allocate_qubit(p, 0, &qubits[i]));
    check(ket_process_apply_gate(p, KET_HADAMARD, 0.0, qubits[i]));
  }

  int steps = (M_PI / 4.0) * sqrt(1 << size);
  for (int i = 0; i < steps; i++) {
    check(ket_process_ctrl_push(p, &qubits[1], size - 1));
    check(ket_process_apply_gate(p, KET_PAULI_Z, 0.0, qubits[0]));
    check(ket_process_ctrl_pop(p));
    for (int j = 0; j < size; j++) {
      check(ket_process_apply_gate(p, KET_HADAMARD, 0.0, qubits[j]));
      check(ket_process_apply_gate(p, KET_PAULI_X, 0.0, qubits[j]));
    }
    check(ket_process_ctrl_push(p, &qubits[1], size - 1));
    check(ket_process_apply_gate(p, KET_PAULI_Z, 0.0, qubits[0]));
    check(ket_process_ctrl_pop(p));
    for (int j = 0; j < size; j++) {
      check(ket_process_apply_gate(p, KET_PAULI_X, 0.0, qubits[j]));
      check(ket_process_apply_gate(p, KET_HADAMARD, 0.0, qubits[j]));
    }
  }

  ket_future_t m;
  check(ket_process_measure(p, qubits, size, &m));

  check(ket_process_prepare_for_execution(p));

  check_kbw(kbw_run_and_set_result(p, KBW_DENSE));

  double exec_time;
  check(ket_process_exec_time(p, &exec_time));

  int64_t value;
  check(ket_future_value(m, &value));

  ket_future_delete(m);
  for (int i = 0; i < size; i++) {
    ket_qubit_delete(qubits[i]);
  }
  free(qubits);
  ket_process_delete(p);

  gettimeofday(&end, NULL);
  double begin_f = begin.tv_sec + (begin.tv_usec * 1e-6);
  double end_f = end.tv_sec + (end.tv_usec * 1e-6);

  printf("%d, %d, %f, %f\n", size, value, exec_time, end_f - begin_f);
}

int main() {
  for (int n = 10; n < 17; n++) {
    grover(n);
  }
}