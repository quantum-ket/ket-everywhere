#include <kbw.h>
#include <libket.h>
#include <stdio.h>
#include <stdlib.h>

void check(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    const char* msg = ket_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

void check_kbw(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    const char* msg = kbw_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

int main() {
  ket_process_t p;
  check(ket_process_new(0, &p));

  ket_qubit_t a, b;
  check(ket_process_allocate_qubit(p, 0, &a));
  check(ket_process_allocate_qubit(p, 0, &b));

  check(ket_process_apply_gate(p, KET_HADAMARD, 0.0, a));

  check(ket_process_ctrl_push(p, &a, 1));
  check(ket_process_apply_gate(p, KET_PAULI_X, 0.0, b));
  check(ket_process_ctrl_pop(p));

  ket_future_t m;
  ket_qubit_t qubits[2] = {a, b};
  check(ket_process_measure(p, qubits, 2, &m));

  check(ket_process_prepare_for_execution(p));

  check_kbw(kbw_run_and_set_result(p, KBW_SPARSE));

  int64_t value;
  check(ket_future_value(m, &value));
  printf("Measurement %d\n", value);

  double time;
  check(ket_process_exec_time(p, &time));
  printf("Execution time: %fs\n", time);

  ket_future_delete(m);
  ket_qubit_delete(a);
  ket_qubit_delete(b);
  ket_process_delete(p);
}