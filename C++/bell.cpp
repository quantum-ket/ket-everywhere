#include <kbw.h>

#include <iostream>
#include <libket.hpp>

void check_kbw(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    auto msg = kbw_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

int main() {
  ket::Process p{0};

  auto a = p.allocate_qubit();
  auto b = p.allocate_qubit();

  p.apply_gate(KET_HADAMARD, 0.0, a);
  p.ctrl({a}, [&]() { p.apply_gate(KET_PAULI_X, 0.0, b); });

  auto m = p.measure({a, b});

  p.prepare_for_execution();

  check_kbw(kbw_run_and_set_result(p.c_process(), KBW_DENSE));

  std::cout << "Measurement: " << m.value() << std::endl;
  std::cout << "Execution time: " << std::fixed << p.exec_time() << "s"
            << std::endl;
}
