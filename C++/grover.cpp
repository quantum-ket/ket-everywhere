#include <kbw.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <libket.hpp>
#include <vector>

void check_kbw(ket_error_code_t code) {
  if (code != KET_SUCCESS) {
    size_t size;
    auto msg = kbw_error_message(code, &size);
    printf("ERROR: %.*s\n", size, msg);
    exit(1);
  }
}

void grover(int size) {
  auto begin = std::chrono::high_resolution_clock::now();

  ket::Process p{static_cast<size_t>(size)};

  std::vector<ket::Qubit> qubits;

  for (int i = 0; i < size; i++) {
    qubits.push_back(p.allocate_qubit());
  }

  for (auto &qubit : qubits) {
    p.apply_gate(KET_HADAMARD, 0.0, qubit);
  }

  auto head = qubits[0];
  std::vector<ket::Qubit> tail{qubits.begin() + 1, qubits.end()};

  int steps = (M_PI / 4.0) * std::sqrt(1 << size);
  for (int i = 0; i < steps; i++) {
    p.ctrl(tail, [&]() { p.apply_gate(KET_PAULI_Z, 0.0, head); });
    for (auto &qubit : qubits) {
      p.apply_gate(KET_HADAMARD, 0.0, qubit);
      p.apply_gate(KET_PAULI_X, 0.0, qubit);
    }
    p.ctrl(tail, [&]() { p.apply_gate(KET_PAULI_Z, 0.0, head); });
    for (auto &qubit : qubits) {
      p.apply_gate(KET_PAULI_X, 0.0, qubit);
      p.apply_gate(KET_HADAMARD, 0.0, qubit);
    }
  }

  auto m = p.measure(qubits);

  p.prepare_for_execution();
  check_kbw(kbw_run_and_set_result(p.c_process(), KBW_DENSE));

  double exec_time = p.exec_time();

  auto end = std::chrono::high_resolution_clock::now();

  std::cout << size << ", " << m.value() << ", " << std::fixed << p.exec_time()
            << ", "
            << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin)
                       .count() *
                   1e-9
            << std::endl;
}

int main() {
  for (int n = 10; n < 17; n++) {
    grover(n);
  }
}