from ket import *
from ket import kbw
from math import sqrt, pi
from time import time


def grover(size: int):
    begin = time()

    qubits = H(quant(size))

    steps = int((pi/4)*sqrt(2**size))
    for _ in range(steps):
        ctrl(qubits[1:], Z, qubits[0])
        with around([H, X], qubits):
            ctrl(qubits[1:], Z, qubits[0])

    print(f"{size}, {measure(qubits).value}, {quantum_exec_time()}, {time()-begin}")


if __name__ == '__main__':
    kbw.use_dense()

    for n in range(10, 17):
        grover(n)
