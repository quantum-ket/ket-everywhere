from ket import *

a, b = quant(2)

ctrl(H(a), X, b)

m = measure(a+b)

print("Measurement", m.value)
print(f"Execution time: {quantum_exec_time():f}s")
