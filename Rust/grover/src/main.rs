use std::{borrow::BorrowMut, f64::consts::PI, ops::Deref};

fn grover(size: i32) -> Result<(), Box<dyn std::error::Error>> {
    let begin = std::time::Instant::now();

    let p = ket::Process::new_ptr();

    let mut qubits = ket::Quant::new(&p, size as usize)?;
    ket::h(&qubits)?;

    let steps = (PI / 4.0 * f64::sqrt((1 << size) as f64)) as i32;

    for _ in 0..steps {
        ket::ctrl(&qubits.slice(1..), || ket::z(&qubits.at(0)))??;

        ket::around(
            &p,
            || {
                ket::h(&qubits).unwrap();
                ket::x(&qubits).unwrap();
            },
            || ket::ctrl(&qubits.slice(1..), || ket::z(&qubits.at(0))),
        )???;
    }

    let m = ket::measure(&mut qubits.borrow_mut())?;

    p.deref().borrow_mut().prepare_for_execution()?;

    kbw::run_and_set_result::<kbw::Dense>(&mut p.deref().borrow_mut())?;

    println!(
        "{}, {}, {}, {}",
        size,
        m.value().unwrap(),
        p.borrow().exec_time().unwrap(),
        begin.elapsed().as_nanos() as f64 * 1e-9
    );

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    for n in 10..17 {
        grover(n)?;
    }

    Ok(())
}
