fn main() -> Result<(), Box<dyn std::error::Error>> {
    let p = ket::Process::new_ptr();

    let a = ket::Quant::new(&p, 1)?;
    let b = ket::Quant::new(&p, 1)?;

    ket::h(&a)?;
    ket::ctrl(&a, || ket::x(&b))??;

    let m = ket::measure(&mut ket::Quant::cat(&[&a, &b])?)?;

    p.borrow_mut().prepare_for_execution()?;

    kbw::run_and_set_result::<kbw::Sparse>(&mut p.borrow_mut())?;

    println!("Measured: {}", m.value().unwrap());
    println!("Execution time: {}s", p.borrow().exec_time().unwrap());

    Ok(())
}
