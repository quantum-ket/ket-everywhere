# Ket Everywhere

This repository features examples using Ket and Libket in different languages, namely C, C++, Rust, and Python.

## Architecture

![Ket Quantum Programming platform architecture](architecture.png)

## Executing

Clone the repository:

```bash
git clone --recursive https://gitlab.com/quantum-ket/ket-everywhere.git
cd ket-everywhere
```

### C and C++

Requirements:

* C and C++ compiler
* CMake 3.10 or newer
* [Rust](https://www.rust-lang.org)

Compile:

```bash
mkdir build
cd build
cmake ..
make
```

Execute:

* C Binaries
  * Bell

    ```bash
    ./bell_c
    ```

  * Grover

    ```bash
    ./grover_c
    ```

* C++ Binaries
  * Bell

    ```bash
    ./bell_cpp
    ```

  * Grover

    ```bash
    ./grover_cpp
    ```

### Python

Requirements:

* [Python 3.7 or newer](https://www.python.org)
* [Ket 0.5 or newer](https://gitlab.com/quantum-ket/ket)

Execute:

* Bell

  ```bash
  python Python/bell.py 
  ```

* Grover:

  ```bash
  python Python/grover.py
  ```

### Rust

Requirements:

* [Rust](https://www.rust-lang.org)

Compile and Execute:

* Bell

  ```bash
  cd Rust/bell
  cargo run
  ```

* Grover

  ```bash
  cd Rust/grover
  cargo run
  ```
